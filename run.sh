#!/bin/sh

TAG=khiemledev/cuda_conda
docker --version
docker build -t "$TAG" .
docker rm "$1"

# Run docker container
docker run -it --gpus all --shm-size 4G --cpus 20 \
  -v /mlcv:/mlcv -v /home:/home \
  --name "$1" \
  "$TAG" bash